function validateName() {
    let name = document.getElementById('name');

    if (!empty(name)) {
        let i = document.getElementById('error-msg-name');
        let input = i.previousElementSibling.firstChild.nextSibling;
        i.innerText = '*No puedes dejar este campo vacío.';
        i.style.display = 'block';
        i.style.width = '80%';
        i.style.color = 'red';
        i.style.fontWeight = '700';
        input.style.border = 'red 1px solid';
        return;
    }
    else {
        let i = document.getElementById('error-msg-name');
        let input = i.previousElementSibling.firstChild.nextSibling;
        i.style.display = 'None';
        input.style.border = 'black 1px solid';
    }

    if (!length(name, 3)) {
        let i = document.getElementById('error-msg-name');
        let input = i.previousElementSibling.firstChild.nextSibling;
        i.innerText = '*El nombre debe tener 3 letras o más.';
        i.style.display = 'block';
        i.style.width = '80%';
        i.style.color = 'red';
        i.style.fontWeight = '700';
        input.style.border = 'red 1px solid';
        return;
    }
    else {
        let i = document.getElementById('error-msg-name');
        let input = i.previousElementSibling.firstChild.nextSibling;
        i.style.display = 'None';
        input.style.border = 'black 1px solid';
    }

    return;
}

function validateEmail() {

}

function validatePassword() {

}

function main() {
    document.getElementById('name').addEventListener('blur', validateName);
    document.getElementById('email').addEventListener('blur', validateEmail);
    document.getElementById('password').addEventListener('blur', validatePassword);
}

main();