function login(){
    location.href = 'login.html';
}

function signIn(){
    location.href = 'sign-in.html';
}

function info(){
    location.href = 'info.html';
}

document.getElementById('login').addEventListener('click', login);
document.getElementById('sign-in').addEventListener('click', signIn);
document.getElementById('info').addEventListener('click', info);