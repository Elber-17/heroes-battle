function empty() {
    for (let argument of arguments) {

        if (argument.value === '') {
            return false;
        }
    }

    return true;
}

function length(input, minimum_length) {
    if (input.value.length < minimum_length) {
        return false;
    }

    return true;
}